/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "armexec.h"
#include "plt_stub.h"

DCLR_STUB(inet_addr)
{
    struct vm *vm = ld->vm;
    char *s = PA(r0);
    return inet_addr((const char *)s);
}

DCLR_STUB(inet_ntoa)
{
    struct vm *vm = ld->vm;
    struct in_addr in = {.s_addr = r0};
    char *s = inet_ntoa(in);

    /* inet_ntoa is non-reentrant nor thread-safe.
     * therefore it is fine to follow the standard here.
     */
    static uint32_t data = 0;
    void *p;

    if (!data)
        data = vm_malloc(vm, 16, &p);
    else
        p = PA(data);

    memcpy(p, s, 16);

    return data;
}

DCLR_STUB(pipe)
{
    struct vm *vm = ld->vm;
    int fds[2];
    int ret;
    int *pfds = PA(r0);

    ret = pipe(fds);

    pfds[0] = fds[0];
    pfds[1] = fds[1];

    return ret;
}

DCLR_STUB(socket)
{
    return socket(r0, r1, r2);
}

DCLR_STUB(close)
{
    int ret;

    ret = close(r0);
    INFO("close: fd:%d, ret:%d\n", r0, ret);

    return ret;
}

DCLR_STUB(getsockname)
{
    struct vm *vm = ld->vm;
    struct sockaddr *addr = PA(r1);
    socklen_t *addrlen = PA(r2);

    /* sizeof(sockaddr_in), sizeof(sockaddr) and sizeof(socklen_t)
     * are platform neutral.
     */

    return getsockname(r0, addr, addrlen);
}

DCLR_STUB(write)
{
    struct vm *vm = ld->vm;
    char *s = PA(r1);
    int ret;

    ret = write(r0, s, r2);
    if (ret == -1)
        perror("write");

    return ret;
}

DCLR_STUB(select)
{
    struct vm *vm = ld->vm;

    struct timeval32 *tm32;
    uint32_t data;

    fd_set *pr, *pw, *pe;
    struct timeval tm;
    struct timeval *ptm;
    int ret;

    pr = pw = pe = NULL;
    tm32 = NULL;
    ptm = NULL;

    if (r1)
        pr = PA(r1);
    if (r2)
        pw = PA(r2);
    if (r3)
        pe = PA(r3);

    data = LW(sp);
    if (data) {
        tm32 = PA(data);
        ptm = &tm;
        ptm->tv_sec = tm32->tv_sec;
        ptm->tv_usec = tm32->tv_usec;
    }

    ret = select(r0, pr, pw, pe, ptm);

    if (ret == -1)
        perror("select");
    else if (ptm) {
        tm32->tv_sec = ptm->tv_sec;
        tm32->tv_usec = ptm->tv_usec;
    }

    return ret;
}

DCLR_STUB(read)
{
    struct vm *vm = ld->vm;
    char *buf = PA(r1);
    int ret;

    ret = read(r0, buf, r2);

    if (ret == -1)
        perror("read");

    return ret;
}

DCLR_STUB(sendto)
{
    /* r0 - sockfd
     * r1 - buf
     * r2 - len
     * r3 - flags
     * [sp] - dest_addr
     * [sp+4] - addrlen
     */
    struct vm *vm = ld->vm;
    uint32_t data;
    const struct sockaddr *dst;
    socklen_t addrlen;
    int fd = r0;
    char *buf = PA(r1);
    int len = r2;
    int flags = r3;
    int ret;
    
    data = LW(sp);
    dst = PA(data);
    addrlen = LW(sp + 4);

    ret = sendto(fd, buf, len, flags, dst, addrlen);
    INFO("sendto: fd:%d, len:%d\n", fd, ret);
    if (ret == -1)
        perror("sendto");

    return ret;
}

DCLR_STUB(send)
{
    struct vm *vm = ld->vm;
    char *s = PA(r1);
    int ret;

    ret = send(r0, s, r2, r3);
    if (ret == -1)
        perror("send");

    return ret;
}

DCLR_STUB(recv)
{
    struct vm *vm = ld->vm;
    char *buf = PA(r1);
    int ret;

    ret = recv(r0, buf, r2, r3);
    if (ret > 0)
        INFO("recv: fd:%d, len=%d\n", r0, ret);
    if (ret == -1)
        perror("recv");

    return ret;
}

DCLR_STUB(bind)
{
    struct vm *vm = ld->vm;
    const struct sockaddr *addr = PA(r1);
    struct sockaddr_in *serv_addr;
    int ret;

    serv_addr = (struct sockaddr_in *) addr;
    INFO("bind: fd=%d, addr=%08x, port=%d\n",
         r0,
         ntohl(serv_addr->sin_addr.s_addr),
         ntohs(serv_addr->sin_port));

    ret = bind(r0, addr, r2);

    if (ret == -1)
        perror("bind");

    return ret;
}

DCLR_STUB(connect)
{
    struct vm *vm = ld->vm;
    const struct sockaddr *addr = PA(r1);
    struct sockaddr_in *serv_addr;
    int ret;

    serv_addr = (struct sockaddr_in *) addr;
    INFO("connect: fd=%d, addr=%08x, port=%d\n",
         r0,
         ntohl(serv_addr->sin_addr.s_addr),
         ntohs(serv_addr->sin_port));

    ret = connect(r0, addr, r2);
    if (ret == -1)
        perror("connect");

    return ret;
}

DCLR_STUB(listen)
{
    int ret;

    ret = listen(r0, r1);
    INFO("listen: fd:%d, ret:%d\n", r0, ret);
    if (ret == -1)
        perror("listen");

    return ret;
}

DCLR_STUB(shutdown)
{
    int ret;

    ret = shutdown(r0, r1);
    INFO("shutdown: fd:%d, ret:%d\n", r0, ret);

    return ret;
}

/* addrinfo is different on 32bit */
struct addrinfo32 {
    int         ai_flags;
    int         ai_family;
    int         ai_socktype;
    int         ai_protocol;
    socklen_t   ai_addrlen;

    /* above is same as 64bit */

    uint32_t    ai_canonname;   /* char *            */
/* Fxxk Bionic!!!!! The ai_addr and ai_canonname is reversed!!!!!! */
    uint32_t    ai_addr;        /* struct sockaddr * */
    uint32_t    ai_next;        /* struct addrinfo * */
};

static struct addrinfo *
addrinfo32_to_64(struct vm *vm, struct addrinfo32 *a32)
{
    struct addrinfo *a64;
    struct sockaddr *addr;
    char *name;

    a64 = calloc(1, sizeof(struct addrinfo));
    a64->ai_flags = a32->ai_flags;
    a64->ai_family = a32->ai_family;
    a64->ai_socktype = a32->ai_socktype;
    a64->ai_protocol = a32->ai_protocol;
    a64->ai_addrlen = a32->ai_addrlen;

    if (a32->ai_addr) {
        addr = PA(a32->ai_addr);
        *a64->ai_addr = *addr;
    }
    if (a32->ai_canonname) {
        name = PA(a32->ai_canonname);
        a64->ai_canonname = name;
    }
    if (a32->ai_next) {
        PANIC("Unhandled link\n");
    }

    return a64;
}

static uint32_t
addrinfo64_to_32(struct vm *vm, struct addrinfo *a64)
{
    struct addrinfo32 *a32;
    struct sockaddr *addr;
    char *name;
    uint32_t data, ret;

    ret = vm_malloc(vm, sizeof(struct addrinfo32), (void **)&a32);
    memset(a32, 0, sizeof(*a32));

    a32->ai_flags = a64->ai_flags;
    a32->ai_family = a64->ai_family;
    a32->ai_socktype = a64->ai_socktype;
    a32->ai_protocol = a64->ai_protocol;
    a32->ai_addrlen = a64->ai_addrlen;

    if (a64->ai_addr) {
        data = vm_malloc(vm, sizeof(struct sockaddr), (void **)&addr);
        memcpy(addr, a64->ai_addr, sizeof(struct sockaddr));
        a32->ai_addr = data;
    }
    if (a64->ai_canonname) {
        data = VSTR(a64->ai_canonname);
        a32->ai_canonname = data;
    }
    if (a64->ai_next) {
        PANIC("Unhandled link\n");
    }

    return ret;
}

DCLR_STUB(getaddrinfo)
{
    struct vm *vm = ld->vm;
    char *node = PA(r0);
    char *service = NULL;
    int ret;
    void *p;

    struct addrinfo *hints = NULL;
    struct addrinfo *res = NULL;

    struct addrinfo32 *hints32 = NULL;
    uint32_t res32;

    /* addrinfo translation */

    if (r1)
        service = PA(r1);
    if (r2) {
        hints32 = PA(r2);
        hints = addrinfo32_to_64(vm, hints32);
    }

    ret = getaddrinfo(node, service, hints, &res);
    if (ret == -1)
        perror("getaddrinfo");

    if (res) {
        res32 = addrinfo64_to_32(vm, res);
        SW(r3, res32);
        freeaddrinfo(res);
    }

    if (hints)
        free(hints);

    return ret;
}

DCLR_STUB(setsockopt)
{
    struct vm *vm = ld->vm;
    const void *opt = PA(r3);
    socklen_t len = LW(sp);
    int ret;

    /* Ignore setsockopt failure, which occurs
     * on IP_ADD_MEMBERSHIP. However, it doesn't seem
     * to matter.
     */
    ret = setsockopt(r0, r1, r2, opt, len);
    if (ret == -1)
        perror("setsockopt");

    return 0;
}

DCLR_STUB(getsockopt)
{
    struct vm *vm = ld->vm;
    void *opt = PA(r3);
    uint32_t data = LW(sp);
    socklen_t *len = PA(data);
    int ret;

    ret = getsockopt(r0, r1, r2, opt, len);
    if (ret == -1)
        perror("getsockopt");

    return ret;
}

DCLR_STUB(fcntl)
{
    int ret;

    switch (r1) {
    case F_DUPFD:
    case F_DUPFD_CLOEXEC:
    case F_SETFL:
    case F_GETFL:
    case F_GETFD:
    case F_SETFD:
        break;

    default:
        PANIC("Unknown fcntl cmd: %d\n", r1);
        break;
    }

    ret = fcntl(r0, r1, r2);

    if (ret == -1) {
        perror("fcntl");
    }

    return ret;
}

DCLR_STUB(ioctl)
{
    struct vm *vm = ld->vm;
    int ret;
    int fd = r0;
    int cmd = r1;
    int *arg = PA(r2);

    switch (r1) {
    case FIONBIO:
    case FIONREAD:
    case SIOCATMARK:
        break;

    default:
        PANIC("Unknown ioctl cmd: %d\n", r1);
        break;
    }

    ret = ioctl(fd, cmd, arg);

    if (ret == -1) {
        perror("ioctl");
    }

    return ret;
}

DCLR_STUB(recvfrom)
{
    /* r0 - fd
     * r1 - buf
     * r2 - len
     * r3 - flags
     * sp - src
     * sp+4 - addrlen
     */
    struct vm *vm = ld->vm;
    char *buf = PA(r1);
    socklen_t *addrlen;
    struct sockaddr *src;
    uint32_t data;
    int ret;

    data = LW(sp);
    src = PA(data);

    data = LW(sp + 4);
    addrlen = PA(data);

    ret = recvfrom(r0, buf, r2, r3, src, addrlen);
    if (ret > 0)
        INFO("recvfrom: fd:%d, len:%d\n", r0, ret);
    if (ret == -1)
        perror("recvfrom");

    return ret;
}

DCLR_STUB(accept)
{
    struct vm *vm = ld->vm;
    struct sockaddr *addr = PA(r1);
    socklen_t *addrlen = PA(r2);
    int ret;

    ret = accept(r0, addr, addrlen);
    INFO("accept: fd:%d, ret:%d\n", r0, ret);
    if (ret == -1)
        perror("accept");

    return ret;
}
